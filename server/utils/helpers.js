const _ = require('lodash')

exports.renameKeysInObject = (myObject, model) => {
  let newObject = {}
  for (const [key, value] of Object.entries(myObject)) {
    if (key in model) {
      newObject[model[key]] = value
    }
  }
  return newObject
}
