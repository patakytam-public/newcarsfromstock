// Routes available for admins (role === 'admin' || role === 'developer')
const Router = require('koa-router')
const router = new Router()
const reader = require('../services/file')
const _ = require('lodash')

router.post('/upload/xls/:type', async (ctx, next) => {
  const file = ctx.request.files[0]
  let result = reader.readXlsWithHeader(file.path)
  ctx.body = result
})
router.post('/upload/image', async (ctx, next) => {
  const file = ctx.request.files[0]
  let result = reader.filesToDir('assets/maci.png', file)
  ctx.body = result
})

// router.use(jwt)

module.exports = router
