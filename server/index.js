const Koa = require('koa')
const mongo = require('koa-mongo')
var body = require('koa-better-body')
var cors = require('koa2-cors')

const consola = require('consola')
const each = require('each-module')
const { Nuxt, Builder } = require('nuxt')

const app = new Koa()
app.use(
  cors({
    origin: function(ctx) {
      const requestOrigin = ctx.get('Origin')
      const whiteList = [
        'http://newcarsfromstock.com',
        'https://newcarsfromstock.com',
        'www.newcarsfromstock.com',
        'http://newcarsfromstock.de',
        'https://v2.newcarsfromstock.de',
        'www.v2.newcarsfromstock.de',
        'http://v2.newcarsfromstock.com',
        'https://v2.newcarsfromstock.com',
        'www.v2.newcarsfromstock.com',
        'http://v2.newcarsfromstock.de',
        'https://v2.newcarsfromstock.de',
        'www.v2.newcarsfromstock.de',
        'http://localhost',
        'https://localhost',
        '0.0.0.0',
        'http://0.0.0.0',
        'https://0.0.0.0'
      ]
      if (whiteList.includes(requestOrigin)) {
        return requestOrigin
      }
      return false
    }
  })
)
/*
app.use(bodyParser())
app.use(koaBody({ multipart: true }))
app.use(ctx => {
  ctx.body = `Request Body: ${JSON.stringify(ctx.request.body)}`
})
*/
// app.use(serve(path.join(__dirname, '/public')))

// can use generator middlewares

app.use(body({ fields: 'body', jsonLimit: '10mb', formLimit: '10mb' }))
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 4000

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(app.env === 'production')

async function start() {
  // Instantiate nuxt.js
  const nuxt = new Nuxt(config)

  // Build in development
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Connect to database
  app.use(
    mongo({
      uri: process.env.MONGO_URL,
      max: 100,
      min: 1
    })
  )

  // Add API routes with prefix: /api + filename
  each('server/controllers', function(id, router, file) {
    router.prefix(`/api/${id}`)
    app.use(router.routes()).use(router.allowedMethods())
  })

  // Add Nuxt
  app.use(ctx => {
    ctx.status = 200 // koa defaults to 404 when it sees that status is unset

    return new Promise((resolve, reject) => {
      ctx.res.on('close', resolve)
      ctx.res.on('finish', resolve)
      nuxt.render(ctx.req, ctx.res, promise => {
        // nuxt.render passes a rejected promise into callback on error.
        promise.then(resolve).catch(reject)
      })
    })
  })

  // Start app
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}

start()
