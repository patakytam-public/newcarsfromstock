const _ = require('lodash')

function validateCollection(collection) {
  let newCollection = []
  for (let item of collection) {
    if (validateObject(item)) newCollection.push(item)
  }
  return newCollection
}
exports.validateCollection = validateCollection

function validateObject(item) {
  let answer = false
  _.forIn(item, (value, key) => {
    if (value !== null) answer = true
  })
  return answer
}
exports.validateObject = validateObject

function toNumberInCollection(collection) {
  for (let item of collection) {
    toNumberInObject(item)
  }
  return collection
}
exports.toNumberInCollection = toNumberInCollection

function toNumberInObject(item) {
  _.forIn(item, (value, key) => {
    if (!_.isNaN(_.toNumber(value))) item[key] = _.toNumber(value)
  })
  return item
}
exports.toNumberInObject = toNumberInObject
