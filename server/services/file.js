const excelToJson = require('convert-excel-to-json')
// var json2xls = require('json2xls')
const fs = require('fs')

exports.readXlsWithHeader = filePath => {
  const result = excelToJson({
    sourceFile: filePath,
    columnToKey: {
      '*': '{{columnHeader}}'
    }
  })
  return result
}

exports.readStock = filePath => {
  const result = excelToJson({
    sourceFile: filePath,
    header: {
      rows: 3
    },
    columnToKey: {
      '*': '{{columnHeader}}'
    },
    sheets: ['Stock']
  })
  return result
}

exports.writeXlsWithHeader = filePath => {
  var json = {
    foo: 'bar',
    qux: 'moo',
    poo: 123,
    stux: new Date()
  }

  var xls = json2xls(json)
  fs.writeFileSync(filePath, xls, 'binary')
}

exports.readJson = filePath => {
  const rawdata = fs.readFileSync(filePath)
  return JSON.parse(rawdata)
}

exports.writeJson = (filePath, data) => {
  const json = JSON.stringify(data, null, 2)
  return fs.writeFile(filePath, json, 'utf8', function(err) {})
}

exports.filesFromDir = filePath => {
  return fs.readdirSync(filePath)
}

exports.filesToDir = (filePath, data) => {
  return fs.writeFile(filePath, data)
}
