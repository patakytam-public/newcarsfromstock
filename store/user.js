// import _ from 'lodash'
// import { auth,  GoogleProvider} from '~/utils/firebase.js'
// import {auth} from '~/utils/firebase.js'
import Cookies from 'js-cookie'

export const state = () => ({
  uid: null,
  user: null,
  cookie: ''
})

// getters---------------------------------------------------------------------------------
export const getters = {
  uid(state) {
    if (state.user) return state.user.uid
    else return null
  },

  user(state) {
    return state.user
  },
  role(state) {
    return !!state.user ? state.user.role : 'guest'
  },
  isAuthenticated(state) {
    return !!state.user
  }
}

// mutations----------------------------------------------------------------------------------
export const mutations = {
  saveUID(state, uid) {
    state.uid = uid
  },
  setUSER(state, user) {
    state.user = user
  }
}

// actions---------------------------------------------------------------------------------
export const actions = {
  async login({ dispatch, commit, state }, data) {
    data.identifier = data.email
    const answer = await this.$db.post('/auth/local', data, { result: false })
    //const token = await auth.currentUser.getIdToken(true)
    let user = answer.user
    if (user) {
      if (user.cookieAccepted) {
        Cookies.set('access_token', answer.jwt) // saving token in cookie for server rendering
      }
      this.$axios.setToken(answer.jwt, 'Bearer')
      commit('setUSER', user)
      commit(
        'updateField',
        { path: 'dialogLogin', value: false },
        { root: true }
      )
      return true
    } else {
      return false
    }
  },

  logout({ commit, dispatch }) {
    // await auth.signOut()

    Cookies.remove('access_token')
    commit('setUSER', null)
    commit('saveUID', null)
  },

  saveUID({ commit }, uid) {
    commit('saveUID', uid)
  },

  setUSER({ commit }, user) {
    commit('setUSER', user)
  }
}
