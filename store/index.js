let maci = ''
import _ from 'lodash'
import { getUserFromCookie } from '~/utils/cookies'
import { getField, updateField } from 'vuex-map-fields'
import VeeValidate, { Validator } from 'vee-validate'
import deVuetify from 'vuetify/es5/locale/de'
import enVuetify from 'vuetify/es5/locale/en'
import huVuetify from 'vuetify/es5/locale/hu'
import deValidate from 'vee-validate/dist/locale/de'
import enValidate from 'vee-validate/dist/locale/en'
import huValidate from 'vee-validate/dist/locale/hu'
import Vue from 'vue'

const vuetifyLang = {
  deVuetify,
  enVuetify,
  huVuetify
}
const validateLang = {
  deValidate,
  enValidate,
  huValidate
}

export const state = () => ({
  navigationDrawer: null,
  indexPage: 'home',
  locale: 'de',
  stockId: false,
  returnPage: null,
  stockId: null,
  selectedModel: null,
  selectedModelId: null,
  selectedVersion: 'all',
  selectedColor: 'all',
  currentBrand: {},
  filteredStocks: [],
  invoiceAddress: {},
  deliveryAddress: {},
  termsAccepted: false,
  userCarts: [],
  userOrders: [],
  forgotToken: null,
  dialogLogin: false,
  dialogUser: false,
  dialogCar: false,
  dialogOrderConfirm: false,
  userPage: null,
  dialogMessage: false,
  cookieAccepted: false,
  showCookieDialog: true,
  message: {
    type: false, // Can be info, error, warning, waiting, confirm
    subtitle: false,
    text: false,
    textToTranslate: false,
    persistent: false
  },
  confirmed: false,
  carGallery: [],
  carGalleryId: null
})

// getters---------------------------------------------------------------------------------
export const getters = {
  getField,
  modelsByKey: state => _.keyBy(state.models, 'id')
  // title: (state, getters, rootState, rootGetters) => (item) => {},
  // items: state => state.items
}

// mutations----------------------------------------------------------------------------------
export const mutations = {
  updateField,
  SET_USER_ORDERS(state, userOrders) {
    state.userOrders = userOrders
  },
  SET_SELECTED_VERSION(state, version) {
    state.selectedVersion = version
  },
  SET_SELECTED_COLOR(state, color) {
    state.selectedColor = color
  },
  SET_CART(state, cart) {
    state.cart = cart
  },
  SET_LOCALE(state, locale) {
    state.locale = locale
  },
  SET_STOCK_ID(state, stockId) {
    state.stockId = stockId
  },
  SET_RETURN_PAGE(state, page) {
    state.returnPage = page
  },
  SET_CURRENT_BRAND(state, brand) {
    state.currentBrand = brand
  },
  SET_BRANDS(state, brands) {
    state.brands = brands
  },
  SET_FILTERED_STOCKS(state, filteredStocks) {
    state.filteredStocks = filteredStocks
  },
  SET_INVOICE_ADDRESS(state, invoiceAddress) {
    state.invoiceAddress = invoiceAddress
  },
  SET_DELIVERY_ADDRESS(state, deliveryAddress) {
    state.deliveryAddress = deliveryAddress
  },
  SET_FORGOT_TOKEN(state, forgotToken) {
    state.forgotToken = forgotToken
  },
  SET_COOKIE_STATEMENT(state, value) {
    state.cookieAccepted = value
  }
}
/*  Examples:
  MY_MUTATION (state, {input1, input2}) {}
  increment (state, payload) {}
*/

// actions----------------------------------------------------------------------------------
export const actions = {
  async nuxtServerInit(
    { dispatch, state, commit, app },
    { req, route, redirect }
  ) {
    // Set user

    let user = {}
    const token = getUserFromCookie(req)
    if (token) {
      // this.$axios.setHeader('Authorization', token)
      this.$axios.setToken(token, 'Bearer')
      let user = await this.$db.get('/users/me')
      user.token = token
      commit('user/setUSER', user)
      if (user && user.cookieAccepted) {
        commit('SET_COOKIE_STATEMENT', true)
      }
      if (user && user.language) commit('SET_LOCALE', user.language)
    } else if (route.path !== '/') {
      // redirect('/')
    }
    dispatch('changeLanguage', state.locale)
  },
  async changeLanguage({ dispatch, commit, state, app }, locale) {
    commit('SET_LOCALE', locale)
    // const vuetifyLanguage = require(`vuetify/es5/locale/${locale}`)
    Vue.prototype.$vuetify.lang.locales[locale] =
      vuetifyLang[`${locale}Vuetify`]
    Vue.prototype.$vuetify.lang.current = locale

    Validator.localize(locale, validateLang[`${locale}Validate`])

    // Default read from file
    // const messages = require(`~/locales/${locale}.json`)

    //Read translates from bucket
    if (this.app.i18n.locale !== locale) {
      this.app.i18n.locale = locale
    }
  }
}
