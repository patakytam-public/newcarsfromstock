export default async function({ query, store, app }) {
  let result
  try {
    result = await app.$db.get(`/auth/forgot-password/validate/${query.auth}`)
    result['token'] = query.auth
  } catch (err) {
    result = { validToken: false }
  }
  store.commit('SET_FORGOT_TOKEN', result)
}
