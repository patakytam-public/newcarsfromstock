export default function({ store, redirect, app }) {
  const role = store.getters['user/role']
  if (role !== 'admin' && role !== 'developer') {
    /*
    if (!store.getters['user/isAuthenticated']) {
      store.dispatch('user/logout')
    }
    */
    return redirect(store.state.returnPage || '/')
  }
}
