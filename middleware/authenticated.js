export default function({ store, redirect }) {
  if (!store.getters['user/isAuthenticated']) {
    store.commit('updateField', { path: 'dialogLogin', value: true })
    return redirect('/')
  }
}
