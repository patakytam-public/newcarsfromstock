import _ from 'lodash'
import flatten from 'flat'
import helper from '~/utils/helpers'
import collection from '~/utils/collection'

export default {
  data: () => ({}),
  computed: {},
  async mounted() {
    await this.init()
  },

  destroyed() {
    this.mounted = false
  },
  methods: {
    selectionUpdate(input) {
      this.newColumns.push({ name: this.search, id: this.search })
      this.columnsToShow = this.convertColumns(this.columnsToShow)
    },
    async init(refreshData) {
      this.mounted = false
      // Load model data
      if (this.modelName) {
        this.model = require(`~/server/models/${this.modelName}`)
      }
      // Load initial data
      if (!this.data && this.collection) {
        this.loadedData = await this.$db.get(`db/${this.collection}`)
      }
      // this.allColumns = this.getAllColumns()
      this.columnsToShow = this.removeObjectColumns()
      if (!_.isEmpty(this.notShow)) {
        let columns = []
        for (let column of this.columnsToShow) {
          if (!this.notShow.includes(column.id)) {
            columns.push(column)
          }
        }
        this.columnsToShow = columns
      }
      this.tableSelectedMethod = this.selectedMethod
      this.tableKeyFields = this.keyFields
      // helper.wait(1000)
      this.mounted = true
      await this.refresh()
    },
    // this converts simple array to array of objects
    // where name is header and id is column id
    convertColumns(columns) {
      let result = []
      if (_.isArray(columns)) {
        for (const column of columns) {
          if (!_.isObject(column)) {
            result.push({ name: column, id: column })
          } else result.push(column)
        }
      } else if (_.isObject(columns)) {
        for (const [key, value] of Object.entries(columns)) {
          result.push({ name: value, id: key })
        }
      }
      return result
    },
    updateData() {
      let table = this.$refs[this.tableRef].table.getSourceData()
      let data = _.cloneDeep(this.allData)
      _.remove(data, this.currentFilter)
      _.remove(table, item =>
        _.every(item, field => _.isEmpty(field) || _.isObject(field))
      )
      data.push(...table)
      this.loadedData = data
    },
    removeObjectColumns() {
      let columns = []
      for (const column of this.allColumnsWithHeader) {
        if (!column.id.includes('.')) {
          columns.push(column)
        }
      }
      return columns
    },
    changeFilterField() {
      this.updateData()
      this.currentFilter = {}
    },
    changeFilterValue(value) {
      this.updateData()
      let filter = {}
      filter[this.selectedFilterField] = value
      this.currentFilter = filter
    },
    getTableData(convertToObject = false) {
      this.updateData()
      let unflattenData = []
      if (this.flatten) {
        _.forIn(this.allData, item =>
          unflattenData.push(flatten.unflatten(item))
        )
      } else unflattenData = this.allData
      if (!_.isEmpty(this.stringsToArray)) {
        for (let item of unflattenData) {
          for (let field of this.stringsToArray) {
            if (_.isString(item[field])) item[field] = item[field].split(',')
          }
        }
      }
      if (this.dataIsObject && convertToObject)
        unflattenData = _.keyBy(unflattenData, 'id')
      return unflattenData
    },
    async upload() {
      let unflattenData = this.getTableData()
      if (this.autoSave) {
        unflattenData = await this.$db.post(
          `/db/${this.collection}/post-differences`,
          unflattenData
        )
        this.loadedData = unflattenData
      } else {
        //if (this.dataIsObject) data = _.keyBy(data, 'id')
        this.$emit('save', unflattenData)
      }
    },
    reload(data = null) {
      this.loadedData = data
    },
    async refresh() {
      // await this.init()
      this.mounted = false
      this.key = `${this.tableRef}-${new Date().getTime()}`
      this.mounted = true
    },
    afterMethodSelected(method) {
      this.refresh()
      /*
      this.keys[this.currentModel] = `${
        this.currentModel
      }-${new Date().getTime()}`
      */
    },
    convertToDbFormat() {
      let data = this.getTableData()
      let header = []
      let result = []
      if (this.tableKeyFields === 'column') {
        data = collection.columnsToRows(data)
      }
      header = data[0]
      data = _.tail(data)
      if (this.model && this.model.converters) {
        header = collection.renameHeader(header, this.model.converters)
      }
      collection.changeValues(['.'], [''], [], header, true)
      data = collection.convertArrayToCollection(header, data)
      this.$emit('beforeConvert', data)
      for (let item of data) {
        collection.toNumberInObject(item)
      }
      this.$emit('afterConvert', data)
      this.loadedData = data
      this.tableSelectedMethod = 'db'
      this.refresh()
    }
  }
}
