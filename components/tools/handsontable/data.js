import _ from 'lodash'
import collection from '~/utils/collection'
import flatten from 'flat'

export default {
  props: {
    collection: {
      type: String,
      default: null
    },
    modelName: {
      type: String,
      default: null
    },
    tableRef: {
      type: String,
      default: 'table'
    },
    columns: {
      type: Array,
      default: () => []
    },
    data: {
      type: [Array, Object],
      default: () => null
    },
    dataIsObject: {
      type: Boolean,
      default: false
    },
    settings: {
      type: Object,
      default: () => {}
    },
    flatten: {
      type: Boolean,
      default: true
    },
    autoSave: {
      type: Boolean,
      default: true
    },
    selectedMethod: {
      type: String,
      default: 'db'
    },
    keyFields: {
      type: String,
      default: 'row'
    },
    stringsToArray: {
      type: Array,
      default: () => []
    },
    notShow: {
      type: Array,
      default: () => []
    },
    //Show elements
    canEmpty: {
      type: Boolean,
      default: false
    },
    buttonTop: {
      type: Boolean,
      default: true
    },
    buttonBottom: {
      type: Boolean,
      default: true
    },
    showColumnSettings: {
      type: Boolean,
      default: true
    },
    showFilters: {
      type: Boolean,
      default: true
    }
  },
  data: () => ({
    mounted: false,
    model: {},
    loadedData: null,
    columnsToShow: [],
    newColumns: [],
    selectedFilterField: 'all',
    selectedFilterValue: '',
    currentFilter: {},
    tableSelectedMethod: 'db',
    tableKeyFields: 'row',
    key: '',
    //Not checked
    search: null,
    allColumns: []
  }),
  computed: {
    allData: {
      get() {
        // Load initial data
        let data = _.cloneDeep(this.data)
        // let data = this.data
        if (this.loadedData) data = this.loadedData
        if (this.dataIsObject) data = _.values(data)
        if (!data) data = []
        // Make flatten data
        let flatData = []
        if (this.flatten) {
          _.forIn(data, item => flatData.push(flatten(item)))
        } else flatData = data
        //this.refresh()
        return flatData
      },
      set() {}
    },
    isEmpty() {
      if (this.tableSelectedMethod === 'original') return true
      return false
    },
    allColumnsWithHeader() {
      let allColumns = []
      // Get columns from props
      if (!_.isEmpty(this.columns)) {
        allColumns = this.convertColumns(this.columns)
        // Get columns from model
      } else if (!_.isEmpty(this.model)) {
        if (this.model.header) allColumns = this.model.header
        else if (this.model.fields)
          allColumns = this.convertColumns(_.keys(this.model.fields))
        // Get columns from data first row
      } else if (this.tableData && !_.isEmpty(this.tableData[0])) {
        allColumns = this.convertColumns(_.keys(this.tableData[0]))
      }
      // Add new columns
      // allColumns.push(...this.newColumns)
      return [...allColumns, ...this.newColumns]
    },
    tableHeader() {
      if (this.isEmpty) return true
      else return collection.getFieldValues('name', this.columnsToShow)
    },
    tableColumns() {
      //this.notShow.includes(column)
      if (this.isEmpty) return []
      const columnsToShow = collection.getFieldValues(
        'name',
        this.columnsToShow
      )
      let columns = []
      for (let field of columnsToShow) {
        if (field === '_id') columns.push({ data: field, readOnly: true })
        else columns.push({ data: field })
      }
      return columns
    },
    tableData() {
      if (this.isEmpty) return []
      else if (this.currentFilter[this.selectedFilterField] === '--empty--') {
        return _.filter(this.allData, item => {
          return _.isEmpty(item[this.selectedFilterField])
        })
      } else return _.filter(this.allData, this.currentFilter)
    },
    tableSettings() {
      let defaultSettings = {
        colHeaders: this.tableHeader,
        rowHeaders: true,
        preventOverflow: 'horizontal',
        contextMenu: true,
        undo: true,
        renderAllRows: true,
        minSpareRows: 3,
        columnSorting: true
      }
      if (!this.isEmpty) {
        defaultSettings.columns = this.tableColumns
        defaultSettings.data = _.cloneDeep(this.tableData)
      }
      this.refresh()
      return _.assign(defaultSettings, this.settings)
    },
    filterValues() {
      let values = ['--empty--']
      if (this.selectedFilterField) {
        const field = this.selectedFilterField
        _.forEach(this.allData, function(item) {
          values.push(item[field])
        })
        return _.uniq(values)
      }
      return values
    }
  }
}
