import _ from 'lodash'
import { mapFields } from 'vuex-map-fields'
// https://www.npmjs.com/package/nuxt-validate
// https://baianat.github.io/vee-validate/

export default {
  props: {
    data: {
      type: Object,
      default: () => {}
    },
    formRef: {
      type: String,
      default: 'form'
    },
    showSaveButton: {
      type: Boolean,
      default: true
    },
    buttonText: {
      type: String,
      default: 'save'
    },
    autoSave: {
      type: String,
      default: null
    },
    showResultDialog: {
      type: Boolean,
      default: true
    }
  },
  data: () => ({
    valid: false,
    form: null
  }),
  computed: {
    ...mapFields(['returnPage'])
  },
  mounted() {
    this.init()
  },
  methods: {
    init() {
      let form = _.cloneDeep(this.initialForm)
      if (this.data) _.assignIn(form, this.data)
      this.form = form
    },
    async submit() {
      let valid = await this.$validator.validateAll()
      if (valid) {
        this.beforeSave()
        if (this.autoSave) {
          const answer = await this.$db.post(autoSave)
          this.afterSave(answer)
        } else {
          const answer = await this.save()
          this.afterSave(answer)
        }
      } else {
        this.afterNotValid()
      }
    },
    beforeSave() {
      this.$emit('beforeSave', this.form)
    },
    save() {
      this.$emit('save', this.form)
      return
    },
    afterSave(answer) {
      this.$emit('afterSave', answer)
      //this.$validator.reset()
    },
    afterNotValid() {
      this.$emit('afterNotValid', this.form)
    },
    async getEmail() {
      let valid = await this.$validator.validate('email')
      if (valid) {
        return this.form.email
      } else {
        return null
      }
    }
  }
}
