# COMPONENTS

**This directory is not required, you can delete it if you don't want to use it.**

The components directory contains your Vue.js Components.

_Nuxt.js doesn't supercharge these components._

Link: [Components Basics — Vue.js](https://vuejs.org/v2/guide/components.html)
