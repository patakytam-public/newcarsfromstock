# TOC

<!-- TOC -->

- [TOC](#toc)
- [Used technologies](#used-technologies)
- [Prerequisites](#prerequisites)
  - [Install node](#install-node)
  - [Install hotel - optional](#install-hotel---optional)
  - [Visual Studio Code Extensions - optional](#visual-studio-code-extensions---optional)
- [Start application](#start-application)
- [Folders in root folder](#folders-in-root-folder)
- [Files in root folder](#files-in-root-folder)
  - [.editorconfig](#editorconfig)
  - [.env](#env)
  - [.env.dev](#envdev)
  - [.eslintrc.js](#eslintrcjs)
  - [.gitignore](#gitignore)
  - [.prettierrc](#prettierrc)
  - [app.json](#appjson)
  - [nuxt.config.js](#nuxtconfigjs)
  - [package.json](#packagejson)
  - [Procfile](#procfile)
  - [yarn.lock](#yarnlock)
- [Installed packages](#installed-packages)
  - [@handsontable/vue](#handsontablevue)
  - [@mdi-font](#mdi-font)
  - [@nuxtjs/axios](#nuxtjsaxios)
    - [Usage](#usage)
    - [Examples](#examples)
  - [cookiparser](#cookiparser)
  - [js-cookie](#js-cookie)
  - [jwt-decode](#jwt-decode)
  - [cross-env](#cross-env)
  - [dotenv](#dotenv)
  - [lodash](#lodash)
  - [nuxt](#nuxt)
    - [Vue loader](#vue-loader)
    - [vue.js](#vuejs)
  - [vuex](#vuex)
    - [usage examples:](#usage-examples)
    - [webpack](#webpack)
    - [Prettier](#prettier)
    - [ESLint](#eslint)
  - [nuxt-validate](#nuxt-validate)
  - [vue-i18n](#vue-i18n)
  - [vuetify](#vuetify)
- [How the app should work](#how-the-app-should-work)

<!-- /TOC -->

# Used technologies

[node.js](https://nodejs.org/en/)  
[vue.js](https://vuejs.org/)  
[nuxt.js](https://nuxtjs.org/)  
[vuetify](https://vuetifyjs.com/en/)  
[markdown](https://en.wikipedia.org/wiki/Markdown)

A ``node.js`` lehetővé teszi webszerverek létrehozását javascript nyelven. A package managere (``npm`` by default, vagy ``yarn``) segítségével telepíthetőek különféle csomagok. A csomagokat telepíthetjük globálisan a  gépre (``yarn global add <package names separated by spaces>``), illetve lokálisan az adott projekthez(az adott projekt root könyvtárában: ``yarn add <package names separated by spaces>``). Ez a projekt root könyvtárában aktualizálja automatikusan a ``package.json`` file-t. A telepített csomagokat a ``node_modules`` folderben tárolja a rendszer. Részleteket ld.  még lentebb a ``package.json`` fejezetben.  

A ``vue.js`` egy node alapú library, ami összehangolja a ``html``, a ``css`` és a ``javascript`` működését, ezzel nagyon egyszerűvé teszi web alkalmazások készítését.  

A ``nuxt.js`` a ``vue.js``-re épülő ``node.js`` alapú library, ami lehetővé teszi ``SSR``(server side rendering) alapú alkalmazások készítését. A ``nuxt.js`` alapból tartalmazhatja a következő csomagokat:

- [axios](https://github.com/nuxt-community/axios-module#readme)
- [vuex](https://vuex.vuejs.org/)
- [webpack](https://webpack.js.org/)
- [vue Loader](https://vue-loader.vuejs.org/guide/)
- [prettier](https://prettier.io/)
- [ESLint](https://eslint.org/)

A csomagokról részletesebben ld. lentebb.  

A ``vuetify`` egy ``Bootstrap``-hoz, hasonló component framework. A komponensek itt azonban ``jquery`` és pure javascript helyett ``vue.js`` alapúak. A design ``material design`` alapú. A framework nagy előnye, hogy támogatja az ``SSR``  programokat is.  

A ``markdown`` egy egyszerű jelölő nyelv, ami megformázza a szövegeket sokkal egyszerűbben, mint a ``html``. A ``github`` és a ``gitlab`` alapból formázottként jeleníti meg az ``.md`` kiterjesztésű fájlokat, illetve minden könyvtárban default-ként egyből megjelenítik a ``README.md`` fájlok tartalmát. Ezel a [linken](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) található egy gyors áttekintés a ``markdown`` nyelvről. A programot minden könytárban ``README.md`` fájlokkal dokumentáljuk. A ``Visual Studio Code``-ba is telepíthetőek ``markdown`` extension-ök.

# Prerequisites

## Install node

```bash
# Install node version manager (nvm)
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash

# Install node version (i.e. 10.10.0) with nvm:
nvm install 10.10.0

# List installed node versions:
nvm ls

# Set node version to use:
nvm use 10.10.0

# Install package manager (yarn):
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update && sudo apt-get install yarn
# yarn info: https://yarnpkg.com/lang/en/
```

## Install hotel - optional

A [hotel](https://github.com/typicode/hotel) egy ``node.js`` csomag, ami lehetővé teszi a gépünkön az alkalmazások egyszerű indítását egy helyről, egy browser-ből.
Install hotel:

```bash
# This will install and start hotel
yarn global add hotel && hotel start
```

A hotel ezek után a browserből a http://localhost:2000/ címen érhető el.

## Visual Studio Code Extensions - optional

```bash
# Contains many packages for vue.js - https://marketplace.visualstudio.com/items?itemName=mubaidr.vuejs-extension-pack

# plugin that autocompletes npm modules in import statements - https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense
code --install-extension christian-kohler.npm-intellisense

# Update packages in package.json to latest version - https://marketplace.visualstudio.com/items?itemName=howardzuo.vscode-npm-dependency
code --install-extension howardzuo.vscode-npm-dependency

# Send http requests and see result in VSC - https://marketplace.visualstudio.com/items?itemName=humao.rest-client
code --install-extension humao.rest-client

# Integrates ESLint into VS Code - https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
code --install-extension dbaeumer.vscode-eslint

# To format your JavaScript / TypeScript / CSS using Prettier. - https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
code --install-extension esbenp.prettier-vscode

# Syntax highlighting for .env files - https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv
code --install-extension mikestead.dotenv

# Auto open preview of markdown files - https://marketplace.visualstudio.com/items?itemName=hnw.vscode-auto-open-markdown-preview
code --install-extension hnw.vscode-auto-open-markdown-preview

# Prettyfy markdown tables - https://marketplace.visualstudio.com/items?itemName=darkriszty.markdown-table-prettify
code --install-extension darkriszty.markdown-table-prettify

# Insert table of content to markdown files - https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-toc
code --install-extension AlanWalk.markdown-toc

# Paste images to markdown files - https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image
code --install-extension telesoho.vscode-markdown-paste-image

# Paste usl to markdown files - https://marketplace.visualstudio.com/items?itemName=kukushi.pasteurl
code --install-extension kukushi.pasteurl

# Use mermaid flowcharts in markdown - https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid
code --install-extension bierner.markdown-mermaid

# Insert excel tables to markdown - https://marketplace.visualstudio.com/items?itemName=csholmq.excel-to-markdown-table
code --install-extension csholmq.excel-to-markdown-table
```

# Start application

After git clone, in application root folder:

```bash
# only first time add app to hotel (optional):
hotel add "yarn dev" --name newcarsfromstock-client

# only first time:
rename .env.dev file to .env

# install dependencies
yarn install

# Start app with hot reload, or instead of it start it from hotel:http://localhost:2000/
$ yarn run dev
```

# Folders in root folder

The folders information are in every folder in ``README.md`` files.

# Files in root folder

## .editorconfig

Passz, gondolom a VSC beállításai a projekthez.

## .env

Ez tartalmazza az environment variable-eket. Benne van a ``.gitignore``-ban, tehát nem megy fel a repo-ba. Az alkalmazás ezt olvassa be.

## .env.dev

Ez tartalmazza az environment variable-eket. Ezt kell átnevezni ``.env``-re. Ez a fájl megy a repo-ba, de nem kerül beolvasásra az alkalmazásbn.

## .eslintrc.js

Ez tartalmazza az ``ESLint`` beállításokat.

## .gitignore

Ami itt meg van adva, az nem megy fel a repo-ba.

## .prettierrc

Ez tartalmazza az ``Prettier`` beállításokat.

## app.json

Ez a deploy-hoz szükséges file.

## nuxt.config.js

Ez tartalmazza a nuxt alapbeállításait. Ezt a file-t olvassa be először a nuxt. Link: [Configuration - Nuxt.js](https://nuxtjs.org/guide/configuration)

## package.json

Ez tartalmazza a szükséges npm csomagokat.

## Procfile

Ez a deploy-hoz szükséges file.

## yarn.lock

Ezt a yarn automatikusan hozza létre, nem tudom mi a szerepe. Még.

# Installed packages

## @handsontable/vue

Spreadsheet on a page. Links: [vue handsontable](https://github.com/handsontable/vue-handsontable-official), [original handsontable](https://handsontable.com/)

## @mdi-font

MDI icons. Links: [Material Design Icons](https://materialdesignicons.com/), [usage with vuetify](https://vuetifyjs.com/en/components/icons#introduction)

## @nuxtjs/axios

Creates http request and handle responses. Link: [Introduction - Axios Module](https://axios.nuxtjs.org/)

### Usage

Component:

`asyncData`

```js
async  asyncData({ app })  { const ip =  await app.$axios.$get('http://icanhazip.com') return  { ip }}
```

`methods`**/**`created`**/**`mounted`**/etc**

```js
methods:  { async  fetchSomething()  { const ip =  await  this.$axios.$get('http://icanhazip.com') this.ip = ip }}
```

Store `nuxtServerInit`

```javascript
async  nuxtServerInit  ({ commit },  { app })  { const ip =  await app.$axios.$get('http://icanhazip.com') commit('SET_IP', ip)}
```

### Examples

```javascript
let answer = await this.$axios.$post('/api/carts', {
  data
})

this.carts = await this.$axios.$get('/api/carts', {
  params
})
```

## cookiparser

Parse `Cookie` header and populate `req.cookies` with an object keyed by the cookie names. Optionally you may enable signed cookie support by passing a `secret` string, which assigns `req.secret` so it may be used by other middleware. Link: [cookie-parser](https://github.com/expressjs/cookie-parser)

## js-cookie

A simple, lightweight JavaScript API for handling browser cookies. Link: [js-cookie](https://github.com/js-cookie/js-cookie)

## jwt-decode

Decode JWT tokens; useful for browser applications. Link: [jwt-decode](https://github.com/auth0/jwt-decode)

## cross-env

Cross platform usage of environment variables. Link: [cross-env](https://www.npmjs.com/package/cross-env)

## dotenv

Read  ``.env`` file and add it's contain to environment variables. Link: [dotenv](https://github.com/motdotla/dotenv#readme)

## lodash

Helper library for for javascript. Link: [Lodash](https://lodash.com/)

## nuxt

Link: [nuxt.js](https://nuxtjs.org/). A ``nuxt`` "csomag" tartalmaz alapból egy rakás hasznos további csomagot is:

### Vue loader

Lehetővé teszi, hogy a ``html``, ``js`` és a ``css`` kód egy fájlban (``.vue``) legyen eltárolva. Link: [vue Loader](https://vue-loader.vuejs.org/guide/).  
A fájl felépítése:

```jsx
<template>
<!-- Ide jön a html kód -->
</template>

<script>
// ide jön a js kód
</script>

<style>
/* ide jön a css */
</style>
```

### vue.js

A nuxt a [vue.js](https://vuejs.org/)-en alapszik.

A ``.vue file`` a ``<script>`` tag-en belül tartalmazza a js kódot. A fájl tartalmazza a ``vue.js`` és a ``nuxt`` séma elemeit is.  
[The Vue Instance — Vue.js](https://vuejs.org/v2/guide/instance.html):
![](assets/markdown/vue-instance.png)

A [Nuxt schema](https://nuxtjs.org/guide#schema):
![](assets/markdown/nuxt-schema.png)

```js
// First comes the imports:
// i.e from other node-modules:
import _ from 'lodash'
// i.e from other files:
import Handsontable from '~/components/handsontable'

// In export dafault comes the parts of vue/nuxt instance
export default {

  // It calles a middleware from middleware folder - https://nuxtjs.org/api/pages-middleware#the-middleware-property
  middleware: 'authenticated',

  // It renders a layout from layouts folder - https://nuxtjs.org/api/pages-layout#the-layout-property
  layout: 'admin',

  //It calles a mixin (the mixin must be imported first) - https://vuejs.org/v2/guide/mixins.html
  mixins: [crudMixin],

  // Used components must be set here. The components must be imported first - https://vuejs.org/v2/guide/components.html#ad
  components: {},

  // Here comes data default values. This data can be shown in html with double curly brackets, i.e: {{ title }}. In js it can be used with this. I.e: this.title
  data: () => ({
    title: 'Page template',
    counter: 0
  }),

  // Here comes data default values if it must be calculated. The function must have a return value. This data can be shown in html with double curly brackets, i.e: {{ test }}. In js it can be used with this. I.e: this.test
  computed: {
    test () { retun 'test' }
  },

  // Here comes the functions can be called from html or from js part
  methods: {
    method1 () { 
      this.counter++     
    }
  },

  created() {
    // this function is called when the page created. See vue instace diagram above.
  },

  mounted() {
    // this function is called when the page mounted. See vue instace diagram above.
  },

  async asyncData ({ store, params, query, route, redirect, req }) {
    let test2 = 'test2'
    // async function get data. in return data can be passed to instance data, then it can be can be shown in html with double curly brackets, i.e: {{ test2 }}. In js it can be used with this. I.e: this.test2
    // https://nuxtjs.org/guide/async-data
     return {
      test2: test2
    }
  }

  async fetch({ store, params, app }) {
     // async function to update vuex store.
     // https://nuxtjs.org/api/pages-fetch#the-fetch-method
  }
}
```

## vuex

State management for ``vue.js`` applications. Link: [Vuex](https://vuex.vuejs.org/)

### usage examples:

```js
// State:
// Call state from root:
computed: {
  // State from root:
  test () { this.$store.state.variableName }
  // State from module:
  test2 () { this.$store.state.moduleName.variableName }
}

// Getters from module
this.$tore.getters['auth/isAuthenticated'] 

// Commit mutation from root:
this.$store.commit('mutation', dataToPass)
// Commit mutation from module:
this.$store.commit('moduleName/mutation', dataToPass)

// Dispatch action from root:
this.$store.dispatch('action', dataToPass)
// Dispatch action from module:
this.$store.dispatch('moduleName/action', dataToPass)
```

### webpack

A fájlokat összecsomagolja, illetve hot-reload (a változatatás azonnal látszódik a browserben frisstés nélkül is) web szerverként is működik.
Link: [webpack](https://webpack.js.org/)

### Prettier

Szabvány a js és CSS fájlok formázásához. A beállítások a ``root`` folderben vannak a ``.prettierrc`` fájlban.
Link: [prettier](https://prettier.io/)

### ESLint

Szabvány a js fájlok formázásához. A beállítások a ``root`` folderben vannak a ``.eslintrc.js`` fájlban.
Link: [ESLint](https://eslint.org/)

## nuxt-validate

Used for form validation. It's based on Veeladite. Links: [lewyuburi/nuxt-validate](https://github.com/lewyuburi/nuxt-validate#readme), [VeeValidate](https://baianat.github.io/vee-validate/)

## vue-i18n

Internationalization package for multilanguage sites. Link: [Vue I18n](https://kazupon.github.io/vue-i18n/)
Example with nuxt: [Internationalization (i18n) - Nuxt.js](https://nuxtjs.org/examples/i18n)

## vuetify

A ``vuetify`` egy ``Bootstrap``-hoz, hasonló component framework. A komponensek itt azonban ``jquery`` és pure javascript helyett ``vue.js`` alapúak. A design ``material design`` alapú. A framework nagy előnye, hogy támogatja az ``SSR``  programokat is.  
Link: [vuetify](https://vuetifyjs.com/en/)  

# How the app should work

```mermaid
graph TD;
    A[user select a car and press add to cart]-->B{Is user logged in?}
    B -->|no| C[Go to login screen];
    C --> D{Is user logged registered?}
    D -->|no| E[Register];
    D -->|yes| F[login];
    B -->|yes| G[Go to shopping cart];
    E --> G
    F --> G
    G --> H[Program removes item from stock]
    G --> I[User press the order button]
    I --> J[Go to order page]
    I --> K[Program sends an email to admin]
    K --> L[Admin confirms the order]
    L --> M[User gets an email]
    L --> N[The order status is updated]
```
