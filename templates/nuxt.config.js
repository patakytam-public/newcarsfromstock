const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['~/assets/style/app.styl', 'handsontable/dist/handsontable.full.css'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    { src: '~/plugins/vue-handsontable.js', ssr: false },
    '~/plugins/i18n.js',
    '~/plugins/collection.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: ['@nuxtjs/axios'],
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    withCredentials: true
  },

  // ======================================================================
  // Build configuration
  // ======================================================================
  // buildDir: './../functions/.nuxt',
  build: {
    analyze: true,
    publicPath: '/assets/',
    extractCSS: true,
    splitChunks: {
      layouts: false
    },
    optimization: {
      splitChunks: {
        name: true
      },
      runtimeChunk: false
    },
    babel: {
      env: {
        production: {
          plugins: ['transform-remove-console']
        }
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {}
  }
}
