// import _ from 'lodash'

export const state = () => ({
})

// getters---------------------------------------------------------------------------------
export const getters = {
  // title: (state, getters, rootState, rootGetters) => (item) => {},
  // items: state => state.items
}

// mutations----------------------------------------------------------------------------------
export const mutations = {}
/* Examples:
  MY_MUTATION (state, {input1, input2}) {}
  increment (state, payload) {}
*/

// actions----------------------------------------------------------------------------------
export const actions = {}
/* Example:
  myAction ({ dispatch, commit, state, rootState, getters, rootGetters  }, {input1, input2}) {
    getters.someGetter // -> 'foo/someGetter'
    rootGetters.someGetter // -> 'someGetter'
    commit('MY_MUTATION', myInput)
    commit(`${table}/SET_${mode}`, payload, { root: true })
    dispatch('myAction', my Input)
    dispatch('api/DB', payload, { root: true })
  } */