const pkg = require('./package')
if (process.env.NODE_ENV !== 'production') {
  // require('dotenv').load()
}

module.exports = {
  mode: 'universal',
  // env: {},
  /*
  ** Headers of the  page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ],
    script: [
      {
        async: true,
        defer: true,
        src: 'https://connect.facebook.net/de_DE/sdk.js'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl',
    '@mdi/font/css/materialdesignicons.css',
    'handsontable/dist/handsontable.full.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    '~/plugins/db.js',
    '~/plugins/i18n.js',
    '~/plugins/collection.js',
    '~/plugins/auth.js',
    '~/plugins/vue-quill.js',
    { src: '~/plugins/carousel.js', mode: 'client' },
    { src: '~/plugins/vue-handsontable', mode: 'client' },
    { src: '~/plugins/vue-cropper', mode: 'client' },
    { src: '~/plugins/vue-tour', mode: 'client' },
    '~/plugins/facebook.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/font-awesome',
    [
      'nuxt-validate',
      {
        //lang: 'hu',
        dictionary: {
          hu: {
            messages: {
              boci: 'kutyi!'
            }
          },
          de: {
            messages: {
              alpha: 'Deutschland'
            }
          }
        }
        // regular vee-validate options
      }
    ]
  ],
  /*
  ** Axios module configuration 
  */
  axios: {
    baseURL: process.env.API_URL || 'http://localhost:1301',
    browserBaseURL: process.env.API_URL || 'http://localhost:1301'
    // prefix: '/api'
    // proxyHeaders: false,
    // credentials: false
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save

      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
        
      }
    }
  }
}
