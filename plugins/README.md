# PLUGINS

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your Javascript plugins that you want to run before mounting the root Vue.js application.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/plugins).

- **collection**: saját készítésű plugin a collection-ök, illetve a http request-ek, response-ok kezeléséhez kezeléséhez. Tervek szerint itt be lesz építve, hogy aktiváljon egy felugró ablakot, hiba illetve várakozás (töltés) esetén. Használata:
```js 
let collection = new this.$Collection
```
- **i18n**: plugin a multilanguage használatához. 
```js 
this.$t('module.field')
```
- **vue-handsontable**: Handsontable plugin, see usage in ``components/handsontable.vue``
- **vuetify**: Link: [Application theming — Vuetify.js](https://vuetifyjs.com/en/style/theme)
