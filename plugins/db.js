import _ from 'lodash'

export default ({ store, app, $axios }, inject) => {
  const db = {
    async axios(method, path, params = {}, data = {}, options = {}) {
      // if (Array.isArray(data)) data = { data: data }
      options = _.assign({ waiting: true, result: false }, options)
      let result
      method = '$' + method
      if (options.waiting) {
        store.commit('updateField', { path: 'dialogMessage', value: true })
        store.commit('updateField', {
          path: 'message',
          value: {
            type: 'waiting', // Can be info, error, warning, waiting, confirm
            persistent: true
          }
        })
      }
      if (method === '$get' || method === '$delete') {
        result = await app.$axios[method](path, {
          params
        })
      } else if (method === '$post' || method === '$put') {
        result = await app.$axios[method](path, data, params)
      } else if (method === '$file') {
        let formData = new FormData()
        formData.append('files', data[0])
        result = await app.$axios.$post(path, formData, {
          headers: { 'Content-Type': 'multipart/form-data' }
        })
      }
      store.commit('updateField', { path: 'dialogMessage', value: false })
      if (
        options.result &&
        // (_.isUndefined(result.ok) || result.ok) &&
        !result.error
      ) {
        store.commit('updateField', { path: 'dialogMessage', value: true })
        store.commit('updateField', {
          path: 'message',
          value: {
            type: 'info', // Can be info,error, warning, waiting, confirm
            // text: result
            textToTranslate: options.textToTranslate || false
          }
        })
      }
      return result
    },
    async get(path, params = {}, options = {}) {
      return await this.axios('get', path, params, {}, options)
    },
    async insert(path, data = {}, options = {}) {
      return await this.axios('post', path, {}, data, options)
    },
    async post(
      path,
      data = {},
      options = { result: true, textToTranslate: 'data_saved' }
    ) {
      return await this.axios('post', path, {}, data, options)
    },
    async file(path, data = {}, options = { waiting: true }) {
      return await this.axios('file', path, {}, data, options)
    },
    async postWithFilter(path, params = {}, data = {}, options = {}) {
      return await this.axios('post', path, params, data, options)
    },
    async put(path, params = {}, data = {}, options = {}) {
      return await this.axios('put', path, params, data, options)
    },
    async delete(path, params = {}, options = {}) {
      return await this.axios('delete', path, params, {}, options)
    }
  }
  inject('db', db)
}
