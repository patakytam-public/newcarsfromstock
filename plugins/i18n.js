import Vue from 'vue'
import VueI18n from 'vue-i18n'
import _ from 'lodash'

Vue.use(VueI18n)

const numberFormats = {
  'de-DE': {
    currency: {
      style: 'currency',
      currency: 'EUR'
    }
  }
}

export default async ({ app, store }) => {
  const locale = store.state.locale
  let messages = await app.$db.get(`/translates`)
  messages = _.keyBy(messages, 'language')

  app.i18n = new VueI18n({
    numberFormats,
    // fallbackLocale: 'de',
    locale: locale,
    messages: {
      de: messages.de.texts,
      en: messages.en.texts,
      hu: messages.hu.texts
    }
  })
  // app.i18n.setLocaleMessage(locale, messages)
  // app.i18n.locale = locale
  // store.dispatch('changeLanguage', locale)
}
