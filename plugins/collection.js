import _ from 'lodash'

export default ({ store, app }, inject) => {
  class Collection {
    constructor(name) {
      this.name = name
      this.answer = false
      this.error = false
    }
    async db({ method = 'get', route = '', params = '', data = '' }) {
      this.clear_log()
      try {
        this.answer = await app.$axios[`$${method}`](route, {
          params,
          data
        })
        return this
      } catch (error) {
        this.error = error
        return this
      }
    }
    clear_log() {
      this.answer = false
      this.error = false
      return this
    }
    result() {
      return this.answer
    }
    async getList(params = '') {
      await this.db({ route: `/api/${this.name}`, params })
    }
  }
  inject('Collection', Collection)
}
