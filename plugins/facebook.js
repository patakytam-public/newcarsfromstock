import Vue from 'vue'

export default ({ store, app, $axios }, inject) => {
  const fb = () => {
    FB.init({
      appId: '501313503731912',
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v3.2'
    })
  }
  inject('fb', fb)
}
