import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    bgdark: colors.grey.darken4,
    bgmedium: '#303030',
    bglight: colors.grey.darken3,
    bglight2: colors.grey.darken2,
    primary: colors.amber.darken2, // a color that is not in the material colors palette
    secondary: colors.grey.darken3,
    third: colors.teal.lighten1,
    info: '#2196f3',
    error: '#ff5252',
    warning: '#fb8c00',
    success: '#4caf50',
    accent: '#ffd277'
  }
})
