export default async ctx => {
  const { store, app } = ctx

  // Check if user is connected
  if (process.server) {
    // await store.dispatch('auth/checkConnectedUser', ctx)
  }
  // Set token to axios
  if (store.state.user.user && store.state.user.user.token) {
    app.$axios.setToken(store.state.user.user.token, 'Bearer')
  }
}
