# PAGES

This directory contains your Application Views and Routes.
The framework reads all the `*.vue` files inside this directory and create the router of your application.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/routing).

# Application
A pages root-ban vannak az oldalak az applikációhoz.
- index - A főoldal, ide érkezik a user
- cart - A kosár megjelenítése
- orders - a rendelések megjelenítése
- auth folder - login és register képernyők

# admin
Ezt használják majd az adminok, a következő tervezett funkciókkal(kialakítás még folyamatban):
- stock feltöltés
- katalógus feltöltés
- modellek beárazása
- rendelések áttekintése, rendelés státuszok változtatása (confirm, close, reject)
- user lista
- mail küldés
- statisztikák megjelenítése

# dev
Ezeket csak a fejlesztő használja, tesztelésre van.