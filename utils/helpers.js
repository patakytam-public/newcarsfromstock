import _ from 'lodash'

//Every input is Array except data. Data is object
export function wait(ms) {
  const start = new Date().getTime()
  let end = start
  while (end < start + ms) {
    end = new Date().getTime()
  }
}

export default {
  wait
}
