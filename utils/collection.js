import _ from 'lodash'

//Every input is Array except data. Data is object
export function changeValues(
  sourceValues,
  targetValues,
  fields,
  data,
  inString = false
) {
  for (let [key, value] of Object.entries(data)) {
    if (fields.includes(key) || _.isEmpty(fields)) {
      for (const [index, sourceValue] of sourceValues.entries()) {
        if (!inString) {
          if (value === sourceValue) data[key] = targetValues[index]
        } else {
          if (_.isString(value)) {
            if (sourceValue === '.') sourceValue = '\\.'
            data[key] = value.replace(new RegExp(sourceValue, 'g'), '')
          }
        }
      }
    }
  }
  return data
}

//
export function convertArrayToCollection(header, data) {
  let result = []
  data.forEach(row => {
    if (!_.isEmpty(row)) {
      let newRow = {}
      row.forEach((item, index) => {
        newRow[header[index]] = item
      })
      result.push(newRow)
    }
  })
  return result
}

export function renameHeader(header, model) {
  let result = []
  header.forEach(item => {
    if (item in model) {
      result.push(model[item])
    } else result.push(item)
  })
  return result
}
export function renameKeysInObject(myObject, model) {
  let newObject = {}
  for (const [key, value] of Object.entries(myObject)) {
    if (key in model) {
      newObject[model[key]] = value
    }
  }
  return newObject
}

export function addConstantToCollection(field, value, data) {
  let result = []
  data.forEach(item => {
    data[field] = value
  })
  return data
}

function toNumberInObject(item, fields = 'all') {
  _.forIn(item, (value, key) => {
    if (
      (fields.includes(key) || fields === 'all') &&
      key !== 'id' &&
      !_.isEmpty(value) &&
      !_.isNaN(_.toNumber(value))
    ) {
      item[key] = _.toNumber(value)
    }
  })
  return item
}

export function columnsToRows(data) {
  let result = []
  data.forEach((row, rowIndex) => {
    if (!_.isEmpty(row)) {
      row.forEach((item, columnIndex) => {
        if (_.isUndefined(result[columnIndex])) result[columnIndex] = []
        result[columnIndex].push(item)
      })
    }
  })
  return result
}

//Get an array from collection with values of specific field
export function getFieldValues(id, data) {
  let result = []
  for (const row of data) {
    result.push(row[id])
  }
  return result
}

export default {
  changeValues,
  convertArrayToCollection,
  renameHeader,
  addConstantToCollection,
  toNumberInObject,
  columnsToRows,
  getFieldValues,
  renameKeysInObject
}
