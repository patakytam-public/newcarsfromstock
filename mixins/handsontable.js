import HandsonTable from '~/plugins/vue-handsontable'
import _ from 'lodash'

export default {
  components: {
    HandsonTable
  },
  data: () => ({
    mounted: false,
    selectedColumn: 'all',
    filterValues: [],
    selectedFilterValue: '',
    currentFilter: {}
  }),
  computed: {
    selectColumns() {
      return ['all', ...this.getHeaderFromData()]
    },

    filteredData() {
      return _.filter(this.data, this.currentFilter)
    },
    standardSettings() {
      let settings = {
        colHeaders: this.empty ? true : this.columnsToShow,
        rowHeaders: true,
        // rowHeaders: ['maci', 'boci'],
        preventOverflow: 'horizontal',
        contextMenu: true,
        undo: true,
        renderAllRows: true,
        minSpareRows: 3,
        columnSorting: true
      }
      if (!this.empty) {
        settings.columns = this.dataToShow
        settings.data = _.cloneDeep(this.filteredData)
      }
      return settings
    }
  },
  mounted() {
    this.wait(1000)
    this.mounted = true
  },
  destroyed() {
    this.mounted = false
  },
  methods: {
    wait(ms) {
      const start = new Date().getTime()
      let end = start
      while (end < start + ms) {
        end = new Date().getTime()
      }
    },
    updateData() {
      let table = this.$refs[this.tableRef].table.getSourceData()
      let data = _.cloneDeep(this.data)
      _.remove(data, this.currentFilter)
      _.remove(table, item => _.every(item, field => _.isEmpty(field)))
      data.push(...table)
      this.data = data
    },
    getAllColumns() {
      if (_.isEmpty(this.columns)) {
        let header = _.keys(this.data[0])
        if (header[0] === '_id') {
          header.push(header.shift())
        }
        return header
      }
      return this.columns
    },
    removeObjectColumns() {
      let columns = []
      for (const column of this.allColumns) {
        if (!column.includes('.') && !this.notShow.includes(column)) {
          columns.push(column)
        }
      }
      return columns
    },
    changeFilterColumn(column) {
      this.updateData()
      this.currentFilter = {}
      let values = []
      _.forEach(this.data, function(item) {
        values.push(item[column])
      })
      this.filterValues = _.uniq(values)
    },
    changeFilterValue(value) {
      this.updateData()
      let filter = {}
      filter[this.selectedColumn] = value
      this.currentFilter = filter
    }
  }
}
